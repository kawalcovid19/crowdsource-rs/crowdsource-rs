import { Injectable } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { BehaviorSubject, of, combineLatest } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, Subject } from 'rxjs';
import { shareReplay, map, switchMap, startWith, take } from 'rxjs/operators';
import { Relawan, PlaceData, toRsData, toPlaceData, RsData, isNumberKey, HistoryValue, ITEM_STATUS } from './rs/rs.component';
import { UserService } from './user.service';

export interface MessageState {
  message: string;
  type?: string; // info | success | warning |  error - info as default
  button?: string | null;
}

@Injectable({ providedIn: 'root' })
export class UtilService {
  // https://simple.wikipedia.org/wiki/Stellar_classification
  SETELLAR_COLOR = [
    '#9db4ff', // 5m blue
    '#aabfff', // 15m deep blue white
    '#cad8ff', // 45m blue white
    '#ffddb4', // 2h pale yellow orange
    '#ffbd6f', // 6h light orange red
    '#f84235', // 20h scarlet
    '#ba3059', // 2d magenta
    '#605170' // dark purple
  ];
  isHandset = false;
  titleSubject = new BehaviorSubject<string>('Beranda');
  title$ = this.titleSubject.asObservable();

  placeDataById$: Observable<{ [placeId: string]: PlaceData }>;
  allPlaceData$: Observable<RsData[]>;
  allUserData$: Observable<Relawan[]>;
  noKpLocation$: Observable<RsData[]>;

  /** Number of different types of notification. */
  notificationCount$: Observable<number>;

  private messageSubject = new Subject<MessageState>();
  messageState = this.messageSubject.asObservable();

  constructor(
    breakpointObserver: BreakpointObserver,
    private firestore: AngularFirestore,
    private userService: UserService,
  ) {
    breakpointObserver
      .observe(Breakpoints.Handset)
      .subscribe(result => this.isHandset = result.matches);

    this.allUserData$ = this.firestore
      .collection<Relawan>('users')
      .valueChanges()
      .pipe(shareReplay(1));

    const faskes$ = this.firestore
      .collection<PlaceData>('rs',
        ref => ref.where('place.types', 'array-contains-any', ['hospital', 'health']))
      .valueChanges()
      .pipe(shareReplay(1));

    this.allPlaceData$ =
      combineLatest(faskes$, this.userService.user$.pipe(startWith(null)))
        .pipe(
          map(([arr, user]) =>
            toRsData(arr.map(placeDataRaw =>
              toPlaceData(placeDataRaw, user)),
              user)),
          shareReplay(1));

    this.placeDataById$ =
      combineLatest(faskes$, this.userService.user$.pipe(startWith(null)))
        .pipe(
          map(([places, user]) => {
            const placeDataById: { [placeId: string]: PlaceData } = {};
            for (const pd of places) {
              const placeData = toPlaceData(pd, user);
              placeDataById[placeData.place.place_id] = placeData;
            }
            return placeDataById;
          }),
          shareReplay(1));

    this.noKpLocation$ =
      this.allPlaceData$.pipe(
        map(places => places.filter(rs => !rs.placeData.kpData)),
        shareReplay(1));

    this.notificationCount$ =
      userService.user$.pipe(
        switchMap(user => {
          return userService.isModerator ? this.noKpLocation$.pipe(
            map(arr => arr.length ? 1 : 0),
            shareReplay(1)) : of(0)
        })
      );
  }

  getColor(ts: number, type: string = 'bg') {
    if (ts) {
      const c = this.SETELLAR_COLOR;
      const ago = (Date.now() - ts) / 1000 / 60;
      for (let i = 0, t = 5; i < c.length; i++, t *= 3) {
        if (ago <= t) {
          if (type == 'bg') return c[i];
          else {
            if (i > 5) return 'inherit';
          }
        }
      }
    }
    return '';
  }

  setTitle(title: string) {
    this.titleSubject.next(title);
  }

  timeAgo(ts: number) {
    const current = Date.now();
    const msPerMinute = 60 * 1000;
    const msPerHour = msPerMinute * 60;
    const msPerDay = msPerHour * 24;
    const msPerMonth = msPerDay * 30;
    const msPerYear = msPerDay * 365;

    var elapsed = current - ts;

    if (elapsed < msPerMinute) return 'beberapa detik yang lalu';
    // return Math.round(elapsed/1000) + ' detik yang lalu';
    else if (elapsed < msPerHour) return Math.round(elapsed / msPerMinute) + ' menit lalu';
    else if (elapsed < msPerDay) return Math.round(elapsed / msPerHour) + ' jam lalu';
    else if (elapsed < msPerMonth) return Math.round(elapsed / msPerDay) + ' hari yang lalu';
    else if (elapsed < msPerYear) return Math.round(elapsed / msPerMonth) + ' bulan yang lalu';
    else return Math.round(elapsed / msPerYear) + ' tahun yang lalu';
  }

  isRsInfoFilled(placeData: PlaceData) {
    return !!placeData.kpData;
    // return placeData.latest?.jenisLayanan?.value && placeData.latest?.statusKepemilikan?.value;
  }

  sendMessage(message: string | MessageState) {
    if (typeof message == 'object') {
      if (!message.button) message.button = 'OK';
      this.messageSubject.next(<MessageState>message);
    } else if (typeof message == 'string') {
      let messageToSend = {
        message: message,
        button: 'OK',
        type: 'info'
      }
      this.messageSubject.next(<MessageState>messageToSend);
    }
  }

  deleteApdOlderThan1Month() {
    combineLatest(this.allPlaceData$, this.userService.user$).pipe(
      take(1)).subscribe(async ([places, user]) => {
        if (!user) return;
        const monthAgo = Date.now() - 31 * 24 * 60 * 60 * 1000;
        console.log(new Date(monthAgo));
        let cnt = 0;
        for (const rsData of places) {
          const value: any = {};
          for (const [k, v] of Object.entries(rsData.placeData.latest)) {
            if (isNumberKey(k)) {
              const h: HistoryValue<number> = v;
              if (h.ts < monthAgo && h.value > 0) {
                h.value = 0;
                h.status = ITEM_STATUS.unset;
                value[k] = h;
              }
            }
          }

          if (Object.keys(value).length === 0) continue;
          console.log(rsData.placeData.place.place_id, value);
          cnt++;
          try {
            const res: any = await this.userService.post(`bulk_edit/${rsData.placeData.place.place_id}`, value);
            if (res.error) console.log(res.error);
          } catch (e) {
            console.error(e);
          }
        }
        console.log(cnt);
      });
  }
}
