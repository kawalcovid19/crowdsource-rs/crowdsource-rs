import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { User, auth } from 'firebase/app';
import { Observable, of, Observer } from 'rxjs';
import { retry, shareReplay, switchMap, filter, take, startWith } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { Relawan, USER_ROLE } from './rs/rs.component';

@Injectable({ providedIn: 'root' })
export class UserService {
  firebaseUser$: Observable<User>;
  user$: Observable<Relawan>;
  user: Relawan;
  userid = '';
  isSignedIn: boolean;

  // In-memory cache for user details.
  relawanCache$: { [uid: string]: Observable<Relawan> } = {};

  isChangingRole = false;

  constructor(
    public afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private http: HttpClient,
  ) {
    this.firebaseUser$ = this.afAuth.user.pipe(shareReplay(1));
    this.user$ = this.firebaseUser$.pipe(
      filter<User>(Boolean),
      switchMap(user => this.db.collection('users').doc<Relawan>(user.uid).valueChanges()),
      switchMap(async user => user ? user : (await this.post('register', {})) as Relawan),
      switchMap(UserService.populateUserLocation),
      shareReplay(1));

    this.user$.subscribe(user => {
      if (!user) return;
      this.userid = user.userid;
      this.user = user;
    });

    this.afAuth.getRedirectResult().then(result => {
      this.isSignedIn = !!result.user;
      // Use result.credential.accessToken to call the Facebook API.
    }).catch(error => {
      console.error(error);
    });
  }

  static populateUserLocation(user: Relawan): Observable<Relawan> {
    if (!user.enable_location) return of<Relawan>(user);
    // This is to immediately signify that location enabled.
    // It will temporarily display wrong location, then it will be corrected soon.
    user.location = { lat: 0, lng: 0 };
    return Observable.create(
      (observer: Observer<Relawan>) => {
        try {
          // Invokes getCurrentPosition method of Geolocation API.
          navigator.geolocation.watchPosition(
            (pos: Position) => {
              console.log('Watch current user position', pos);
              user.location = {
                lat: pos.coords.latitude,
                lng: pos.coords.longitude
              }
              observer.next(user);
            },
            (error: PositionError) => {
              console.log('Geolocation service: ' + error.message);
              user.location = null;
              observer.next(user);
            }
          );
        } catch (error) {
          console.log('Geolocation error: ' + error.message);
          user.location = null;
          observer.next(user);
        }
      })
      .pipe(startWith(user));
  }

  get role() {
    return this.user && USER_ROLE[this.user.role];
  }

  get isRegistrationInfoCompleted() {
    const r = this.user?.registration;
    return !!(r && r.regEmail && r.profesi && r.noHp && r.instansi && r.fotoIdCard && r.deskripsi);
  }

  get isPublic() {
    return this.user && this.user.role >= USER_ROLE.PUBLIC;
  }

  get isRelawan() {
    return this.user && this.user.role >= USER_ROLE.RELAWAN;
  }

  get isModerator() {
    return this.user && this.user.role >= USER_ROLE.MODERATOR;
  }

  async loginFacebook() {
    try {
      var provider = new auth.FacebookAuthProvider();
      await this.afAuth.signInWithRedirect(provider);
    } catch (e) {
      alert(e);
    }
  }

  getUser$(uid: string): Observable<Relawan> {
    if (this.relawanCache$[uid]) return this.relawanCache$[uid];
    return this.relawanCache$[uid] =
      this.user$.pipe(
        switchMap(_ => this.db
          .collection('users')
          .doc<Relawan>(uid)
          .valueChanges()),
        shareReplay(1));
  }

  async changeRole(user: Relawan, role: USER_ROLE) {
    try {
      this.isChangingRole = true;
      const res: any = await this.post('change_role', { uid: user.userid, role });
      if (res.error) alert(res.error);
      this.isChangingRole = false;
      return res;
    } catch (e) {
      alert(e.message);
    }
    this.isChangingRole = false;
  }

  static API_PREFIX = 'https://us-central1-kawalcovid19.cloudfunctions.net/api';

  async get<T>(path: string) {
    const url = `${UserService.API_PREFIX}/${path}`;
    return this.http
      .get<T>(url, await this.getHeaders())
      .pipe(retry(3), take(1))
      .toPromise();
  }

  async post<T>(path: string, body: any) {
    const url = `${UserService.API_PREFIX}/${path}`;
    return this.http
      .post<T>(url, body, await this.getHeaders())
      .pipe(retry(3), take(1))
      .toPromise();
  }

  private async getHeaders() {
    const user = await this.firebaseUser$.pipe(take(1)).toPromise();
    const idToken = await user.getIdToken();
    const headers = { Authorization: `Bearer ${idToken}` };
    return { headers };
  }

  async logout() {
    await this.afAuth.signOut();
    location.reload();
  }
}
