import { Component, AfterContentInit, Input, Output, OnChanges, SimpleChange, SimpleChanges, EventEmitter } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import * as d3 from 'd3-scale-chromatic';
import { ColorGenerator } from '../color-generator';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html'
})
export class BarChartComponent implements AfterContentInit, OnChanges {
  public options: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    // scales: { xAxes: [{}], yAxes: [{}] },
    scales:{
      xAxes: [{}],
      yAxes: [{
            display: true,
            ticks:{
              beginAtZero:true
            }
        }]
    },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    },
    tooltips: {
					mode: 'index',
					callbacks: {
						// Use the footer callback to display custom data items showing in the tooltip
						footer: function(tooltipItems, data) {
							var items = "\n";
              tooltipItems.forEach(function(tooltipItem) {
								let index = tooltipItem.index;
                data.datasets.forEach(function(item:any){
                  item.tooltipsData[index].forEach(function(x){
                    items += `${x.name}: ${x.count}  ${x.unitLabel}\n`;
                  })
                })
							});
              items+= '\nKlik untuk lebih rinci';
							return items;
						},
					},
					footerFontStyle: 'normal'
				},
  };
  @Input() labels:Label[];
  @Input() dataSets:ChartDataSets[];
  @Input() legend:boolean = true;
  public type: ChartType = 'bar';
  public plugins = [pluginDataLabels];
  public colors:any[];
  isReady:boolean = false;
  @Output() barClick = new EventEmitter<{}>();

  constructor(private colorGenerator: ColorGenerator) { }

  ngAfterContentInit() {
    this.colors = [{
      backgroundColor:this.colorGenerator.interpolateColors(
        this.dataSets.length,
        d3.interpolateInferno,
        {
          colorStart: 0,
          colorEnd: 1,
          useEndAsStart: false,
        }
      )
    }]
    this.isReady = true
  }
  ngOnChanges(changes: SimpleChanges) {
    const dataSets: SimpleChange = changes.dataSets;
    const labels: SimpleChange = changes.labels;
    this.dataSets = dataSets.currentValue
    this.labels = labels.currentValue
  }
  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    if(Array.isArray(active)){
      if(active.length == 1 && active[0]['$datalabels'] && active[0]['$datalabels'][0]['$context']['dataset']){
        this.barClick.emit({
          selectedIndex: active[0]['$datalabels'][0]['$context']['dataIndex'],
          dataSets: active[0]['$datalabels'][0]['$context']['dataset'],
          label: active[0]['_view']['label']
        })
      }
    }
  }

}
