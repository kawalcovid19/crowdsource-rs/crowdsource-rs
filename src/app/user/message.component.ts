import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { MessageState, UtilService } from '../util.service';
import { Subscription } from 'rxjs';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarVerticalPosition} from '@angular/material/snack-bar';

@Component({
  selector: 'app-message',
  template: `<div></div>`,
  encapsulation: ViewEncapsulation.None,
  styles:[`
    .snackbar-message{
      z-index:2001;
    }
    .snackbar-message .mat-simple-snackbar{
      color:#fff;
    }
    .snackbar-message .mat-simple-snackbar .mat-simple-snackbar-action{
      border-color:#20c997;
      border-width: 1px;
      border-style: solid;
      border-radius: .25rem;
      transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .snackbar-message .mat-simple-snackbar .mat-simple-snackbar-action button{
      color:#ffc107;
      font-weight: bold;
    }
    .snackbar-message-info{
      background-color: #3389FE;
    }
    .snackbar-message-success{
      background-color: #219653;
    }
    .snackbar-message-warning{
      background-color: #F2994A;
    }
    .snackbar-message-error{
      background-color: #D8232A;
    }
  `]
})
export class MessageComponent implements OnInit, OnDestroy {
  message:any;
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  private subscription: Subscription;
  constructor(private utilService: UtilService, public snackBar: MatSnackBar) { }//public snackBarConfig: MatSnackBarConfig

  ngOnInit() {
    this.subscription = this.utilService.messageState
      .subscribe((state: MessageState) => {
        let config = new MatSnackBarConfig();
        config.verticalPosition = this.verticalPosition;
        config.duration = 4000;
        config.panelClass = ['snackbar-message', `snackbar-message-${state.type}`];

        this.snackBar.open(state.message, state.button, config);

      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
